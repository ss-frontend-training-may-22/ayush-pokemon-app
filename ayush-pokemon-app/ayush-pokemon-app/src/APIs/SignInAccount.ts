import { SignInType } from "../Models";
import { post } from "../Util/ApiManager";
import { signInApi } from "../Util/Endpoint";

const signInAccount = (data: SignInType) => {
  return post(signInApi(), {data});
};
export { signInAccount };
