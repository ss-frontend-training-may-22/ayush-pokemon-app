import { SignUpType } from "../Models";
import { post } from "../Util/ApiManager";
import { signUpApi } from "../Util/Endpoint";
const newAccountData = ( data: SignUpType) => {
  return post(signUpApi(), data);
};
export { newAccountData };
