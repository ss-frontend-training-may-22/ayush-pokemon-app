import { useState } from 'react'
import { Route, Routes } from 'react-router-dom'
import {LoginPage, PokemonDetail, PokemonListing, RegisterPage} from './Pages'

function App() {
  const localData = localStorage.getItem("login");
  const [loginState, setLoginState] = useState(localData === "true" ? true : false);
  return ( 
      <>
       <Routes>
        {loginState ?
          <>
            <Route path='/viewSinglePokemonRecord/:id' element={<PokemonDetail />} />
            <Route path='/' element={<PokemonListing />} />
          </>
          :
          <>
            <Route path='/' element={<LoginPage loginState={loginState} setLoginState={setLoginState} />} />
            <Route path='/register' element={<RegisterPage />} />
          </>
        }

      </Routes>

      </>
    )
}

export default App
