import { useContext } from "react";
import { LoginContext } from "../Context/AuthContext";
import { AUTHCONTEXT_ERROR_MESSAGE } from "../Util/constants";

const useAuthContext = () => {
  const context = useContext(LoginContext);

  if (!context) {
    throw Error(AUTHCONTEXT_ERROR_MESSAGE);
  }
  return context;
};

export default useAuthContext;
