type LoginStateFunctionType = {
  loginState: boolean;
  setLoginState: (loginState: boolean | ((prevVar: boolean) => boolean)) => void;
};
export {type LoginStateFunctionType};