type RegisterType = {
  email?: string,
  password?: string,
  confirmpassword?: string
}
export {type RegisterType};