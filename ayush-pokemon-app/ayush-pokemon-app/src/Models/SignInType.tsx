type SignInType = {
  email: string,
  password: string
}
export {type SignInType};
