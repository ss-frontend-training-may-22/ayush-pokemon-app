type SignUpType = {
  email: string,
  firstName: string,
  lastName: string,
  password: string,
  confirmPassword:string
}
export {type SignUpType};