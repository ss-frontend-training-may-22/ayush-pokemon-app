type SignInTokenType = {
  status: string,
  token: string
}

export {type SignInTokenType}; 