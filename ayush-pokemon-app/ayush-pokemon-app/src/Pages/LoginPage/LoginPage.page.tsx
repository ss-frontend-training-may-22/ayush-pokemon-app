import { Form, Field } from 'react-final-form'
import { Link, useNavigate } from 'react-router-dom'
import { AuthType, LoginStateFunctionType, SignInType } from '../../Models'
import { ButtonComponent, InputComponent } from '../../stories'
import { composeValidators, mustBeEmail, required } from '../../Util/Validation'
import style from './LoginPage.module.scss'


function LoginPage({ loginState, setLoginState }: LoginStateFunctionType) {
        const navigate = useNavigate();
    
        const onSubmit = (e: AuthType) => {
            const UsersDetail = localStorage.getItem('users');
    
            if (UsersDetail) {
                const Users = JSON.parse(UsersDetail);
                Users.map((user: SignInType) => {
                    if (user.email === e.email && user.password === e.password) {
                        localStorage.setItem("login", (!loginState).toString());
                        setLoginState(!loginState);
                        navigate('/');
                    }
                })
            }
        }
    
        const validate = (e?: AuthType) => {
            const errors: AuthType = {};
            if (!e?.email) {
                errors.email = 'Email Field is Empty'
            }
            if (!e?.password) {
                errors.password = 'Password Field is Empty'
            }
    
            return errors;
        }
    
    
    return (
        <>
        <div className={style.form}>
                <h2>Login</h2>
        <Form
                    onSubmit={onSubmit}
                    validate={validate}
                    render={({ handleSubmit }) => (
                        <form onSubmit={handleSubmit}>
                            <div className={style.form__formcontrol}>
                                <Field name='email'
                                >
                                    {({ input, meta }) => (
                                        <div>
                                        <label>Email</label>
                                        <InputComponent
                                            inputLabel='Email'
                                            inputType='email'
                                            inputColor='primary'
                                            hasFullWidth={true}
                                            {...input} />
                                        {meta.error && meta.touched && (
                                <span className={style.error}>
                                  {meta.error}
                                </span>
                              )}
                                    </div>
                                    )}
                                </Field>
                                </div>

                                
                            <div className={style.form__formcontrol}>
                                <Field name='password' >
                                    {({ input, meta }) => (
                                        <div>
                                            <label>Password</label>
                                            <InputComponent
                                                inputLabel='Password'
                                                inputType='password'
                                                inputColor='primary'
                                                hasFullWidth={true}
                                                {...input} />
                                            {meta.error && meta.touched && (
                                <span className={style.error}>
                                  {meta.error}
                                </span>
                              )}
                                        </div>
                                    )}
                                </Field>
                            </div>

                            <div className={style.form__formcontrol}>
                                <ButtonComponent
                                    variant={'contained'}
                                    label={'Login'}
                                    type={'submit'} />
                            </div>

                            <Link to={'/register'}>New user? Register</Link>   
        </form>
                    )}
                />
        </div>

        </>
    )
}

export default LoginPage
