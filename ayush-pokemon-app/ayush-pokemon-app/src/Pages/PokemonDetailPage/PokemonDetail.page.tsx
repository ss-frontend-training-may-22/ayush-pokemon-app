import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom';
import { ButtonComponent } from '../../stories';
import PokemonDetailStyle from "./PokemonDetail.module.scss";
function PokemonDetail() {
  const parentId: number = Number(useParams().id);
  const [data,setData] = useState();
  useEffect(() => {
    axios.get(`https://pokeapi.co/api/v2/pokemon/${parentId}`).then(function (response) {
      setData(response.data);     
    }); 
  }, []);

  const handleClick = () => {
    history.back()
  }

  return (
    <>
    <div className={PokemonDetailStyle.showPokemons}>
    <div className={PokemonDetailStyle.showPokemons__navbar}>
    <h1>Welcome to Pokemon Application</h1>
    </div>
   
    <h1>Pokemon Details</h1>
    <span><ButtonComponent type='button' variant='contained' label={"Go Back"} onClick={handleClick}/></span>
    <div>
    <img src={data?.sprites?.other?.dream_world.front_default} alt="" />
    <div className={PokemonDetailStyle.showPokemons__details}>
      <div ><h3>Name:</h3>{data?.name}</div>
      <div ><h3 >Height:</h3>{data?.height}</div>
      <div ><h3 >Weight:</h3>{data?.weight}</div>
      <div ><h3>Base Experience:</h3>{data?.base_experience}</div>
      <div ><h3 >Order:</h3>{data?.order}</div>
      <div ><h3 >List of abilities:</h3></div>
      <ol >
      {
          data?.abilities?.map((abiliti,index)=>{
            return (
              <li>{index+1}. {abiliti?.ability?.name} </li>
            )
          })
        }
        </ol>
        </div>
</div>
</div>
    </>
  )
}

export default PokemonDetail;
