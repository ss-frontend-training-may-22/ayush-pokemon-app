import axios from 'axios';
import { useEffect, useState } from 'react';
import CardComponent from '../../stories/Card/Card.component';
import StylePokemonListing from "./PokemonListing.module.scss";
import ButtonComponent from '../../stories/Button/Button.component';
import SearchIcon from '@mui/icons-material/Search';
import DropDownComponent from '../../stories/DropDown/DropDown.component';
import { Navigate } from 'react-router-dom';

export type PokemonsPropsType = {
 name:string,
 url:string
};

export type PokemonsDetailsPropsType = {
  id:number;
  name:string,
  height:number,
  weight:number,
  listOfAbility:string[],
  imageURL:string
 };

function PokemonListing() {
  const [pokemons, setPokemons] = useState<PokemonsPropsType[]>([]);

  useEffect(() => {
    axios.get("https://pokeapi.co/api/v2/pokemon").then(function (response) {
      setPokemons(response.data.results);
    }); 
  }, []);

  return (
    <>
<div className={StylePokemonListing.showPokemons}>
<div className={StylePokemonListing.showPokemons__navbar}>
<h1>Welcome to Pokemon Application</h1>
</div>
<div className={StylePokemonListing.showPokemons__functionality}>
<input type="text" placeholder='Search...'/>
<span className={StylePokemonListing.showPokemons__icon}><ButtonComponent type='button' variant='contained' label={<SearchIcon/>}/></span>
<span><DropDownComponent size='small'/></span>
</div>
          <div className={StylePokemonListing.showPokemons__gridLayout}>
      {pokemons.map((item) => {
              return (     
                <>     
                    <CardComponent {...item}/>
                </>
              );
            })}
     </div>
   </div> 
     </>
  )
}

export default PokemonListing;
