import { Form, Field } from 'react-final-form'
import { Link, useNavigate } from 'react-router-dom'
import { RegisterType } from '../../Models'
import { InputComponent, ButtonComponent } from '../../stories'
import {mailformat} from '../../Util/Validation'
// import { emailRegex, passwordRegex } from '../../Util/Constans'
import style from './RegisterPage.module.scss'

function RegisterPage() {
    const navigate = useNavigate();
    const passwordRegex =  [/([a-zA-Z]*[0-9]+)/];
    const onSubmit = (e: RegisterType) => {
      const newUser = { email: e.email, password: e.password };
        const registeredUsers = localStorage.getItem('users');
       
        if (registeredUsers) {
            const addNewUser = JSON.parse(registeredUsers);
            addNewUser.push(newUser);
            localStorage.setItem('users', JSON.stringify(addNewUser))
        } else {
            const newUsers: RegisterType[] = [];
            newUsers.push(newUser);
            localStorage.setItem('users', JSON.stringify(newUsers))
        }
        navigate('/');
    }

    const validate = (e?: RegisterType) => {
        const errors: RegisterType = {};

        if (!e?.email) {
            errors.email = 'Please enter email'
        } else {
            if (mailformat.test(e.email) === false || e.email.length <= 4 || e.email.length > 100) {
                errors.email = 'Enter Valid Email'
            }
        }

        if (!e?.password) {
            errors.password = 'Password field is empty'
        } else {
          // passwordRegex.test(e.password) === false ||
            if (e.password.length <= 4 || e.password.length >= 16) {
                errors.password = 'Password length must be between 5 to 15 character and should contain 1 Digit'
            }
        }

        if (!e?.confirmpassword) {
            errors.confirmpassword = 'Password field is empty'
        } else {
            if (e?.password !== e?.confirmpassword) {
                errors.confirmpassword = 'Password does not match with confirm-password'
            }
        }

        return errors;
    }

    return (
            <div className={style.form}>
                <h2>Register</h2>
                <Form
                    onSubmit={onSubmit}
                    validate={validate}
                    render={({ handleSubmit }) => (
                        <form onSubmit={handleSubmit}>
                            <div className={style.form__formcontrol}>
                                <Field name='email'>
                                    {({ input, meta }) => (
                                        <div>
                                            <label>Email</label>
                                            <InputComponent
                                                inputLabel='Email'
                                                inputType='email'
                                                inputColor='primary'
                                                hasFullWidth={true}
                                                {...input} />
                                            {meta.error && meta.touched && <span className={style.errorMessage}>{meta.error}</span>}
                                        </div>
                                    )}
                                </Field>
                            </div>

                            <div className={style.form__formcontrol}>
                                <Field name='password'>
                                    {({ input, meta }) => (
                                        <div>
                                            <label>Password</label>
                                            <InputComponent
                                                inputLabel='Passowrd'
                                                inputType='password'
                                                inputColor='primary'
                                                hasFullWidth={true}
                                                {...input} />
                                            {meta.error && meta.touched && <span className={style.errorMessage}>{meta.error}</span>}
                                        </div>
                                    )}
                                </Field>
                            </div>

                            <div className={style.form__formcontrol}>
                                <Field name='confirmpassword'>
                                    {({ input, meta }) => (
                                        <div>
                                            <label>Re-Enter Password</label>
                                            <InputComponent
                                                inputLabel='Confirm Password'
                                                inputType='password'
                                                inputColor='primary'
                                                hasFullWidth={true}
                                                {...input} />
                                            {meta.error && meta.touched && <span
                                                className={style.errorMessage}>{meta.error}</span>}
                                        </div>
                                    )}
                                </Field>
                            </div>

                            <div className={style.form__formcontrol}>
                                <ButtonComponent
                                    variant={'contained'}
                                    label={'Register'}
                                    type={'submit'} />
                            </div>
                        </form>
                    )}
                />

                <Link to={"/"}>Login</Link>
            </div>
    )
}

export default RegisterPage
