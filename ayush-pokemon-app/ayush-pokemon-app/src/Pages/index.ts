import { RegisterPage } from "./RegisterPage";
import { LoginPage } from "./LoginPage";
import { PokemonListing } from "./PokemonListingPage";
import { PokemonDetail } from "./PokemonDetailPage";

export {RegisterPage, LoginPage, PokemonDetail, PokemonListing};