import axios, { AxiosRequestConfig } from "axios";

export const get = <T,>(url: string, data?:AxiosRequestConfig) => {
  return axios.get(url, data);
};

export const post = <T,>(url: string, data?: AxiosRequestConfig) => {
  return axios.post(url,data.data,{params:data.params});
};

export const put = <T,>(url: string, data?: AxiosRequestConfig) => {
  return axios.put(url, data.data, {params:data.params});
};

export const remove = <T, >(url: string,  data?: AxiosRequestConfig) => {
  return axios.delete(url, data);
};
