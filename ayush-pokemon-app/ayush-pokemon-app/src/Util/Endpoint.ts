import { BASE_URL } from "./constants";

export const signUpApi = () => `${BASE_URL}/user/signup`;
export const signInApi = () => `${BASE_URL}/user/signIn`;
export const sessionGenerationApi = () =>`${BASE_URL}/order/create-checkout-session`;
