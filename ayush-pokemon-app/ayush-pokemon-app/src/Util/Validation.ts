const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const required = (value: string) => (value ? undefined : 'Required')
const mustBeEmail = (value: string) => (!mailformat.test(value) ? 'Must be a email format' : undefined)
const composeValidators = (...validators) => (value: string) =>
      validators.reduce((error, validator) => error || validator(value), undefined)
const mustBePositiveNumber = (value: number) =>
        (value && value > 0 ? undefined : 'Must required and should be a positive')
export {mailformat, required, mustBePositiveNumber, mustBeEmail, composeValidators };
