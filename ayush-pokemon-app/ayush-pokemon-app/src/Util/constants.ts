export const BASE_URL = "https://limitless-lake-55070.herokuapp.com";
export const NO_DATA_FOUND_MESSAGE = "No login data found";
export const SEARCH_ITEMS_MESSAGE = "  Search Items";
export const CREATE_ACCOUNT_MESSAGE = "Create Your Simplecoding Account";
export const SIGNIN_HERE_MESSAGE = "Signin Here";
export const ACCOUNT_CREATED_MESSAGE = "Account Created Successfully";
export const ERROR_IN_ACCOUNT_CREATE_MESSAGE = "Account Was Not Created";
export const PASSWORD_DOES_NOT_MATCH_MESSAGE =
  "Password and confirm password does not match";
export const AUTHCONTEXT_ERROR_MESSAGE =
  "useAuthContext used outside auth context";

export const Footer_LINKS = [
  "About Us",
  "Anroid App",
  "iOS App",
  "Facebook",
  "Twitter",
  "Instagram",
  "Sell with us",
  " Become an Affiliate",
  "Advertise Your Products",
  "Return Centre",
  "100% Purchase Protection",
  "Help",
  "App Download",
];
