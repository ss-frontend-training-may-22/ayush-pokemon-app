import { Button } from '@mui/material'

export type ButtonType = {
    variant: "text" | "outlined" | "contained" | undefined
    label: any,
    onClick?: React.MouseEventHandler,
    type: 'submit' | 'reset' | 'button'
}

function ButtonComponent({ variant, label, onClick, type }: ButtonType) {
    return (
        <>
            <Button
                variant={variant}
                onClick={onClick}
                type={type}>
                {label}
            </Button>
        </>
    )
}

export default ButtonComponent
