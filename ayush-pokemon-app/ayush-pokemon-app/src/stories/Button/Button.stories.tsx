import { ComponentStory, ComponentMeta } from "@storybook/react";
import ButtonComponent from "./Button.component";
import SearchIcon from '@mui/icons-material/Search';

export default {
  title: "ButtonComponent",
  component: ButtonComponent,
  argTypes: {
    onClick: { action: "Clicked" },
  },
} as ComponentMeta<typeof ButtonComponent>;

const Template: ComponentStory<typeof ButtonComponent> = (args) => (
  <ButtonComponent {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  variant: "contained",
  type:'button',
  label: ""
};