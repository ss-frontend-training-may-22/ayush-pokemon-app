import { CardContent, CardMedia, Typography } from "@mui/material";
import axios from "axios";
import { useEffect } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
// import { Link } from "react-router-dom";
import StyledCard from "./Card.styled";

type cardComponentProps = {
  name: string;
  url:string;
  onClick?: React.MouseEventHandler;
};

const CardComponent = ({
  name,
  url,
  onClick,
}: cardComponentProps) => {
  const navigate = useNavigate();
  const [data, setData] = useState({});
  useEffect(() => {
    axios.get(url).then(function (response) {
      setData(response.data)})
    },[]);

    const handleClick=(id:number)=>{
      navigate(`/viewSinglePokemonRecord/${id}`);
    }
  
  return (
    <StyledCard onClick = {()=>handleClick(data.id)}>
      <CardMedia
        component="img"
        image={data.sprites?.other?.dream_world.front_default
        }
        alt="green iguana"
      />
      <CardContent classes={{root:"CustomRoot"}}>
        <div className="detail">
        <div><h3>Name:</h3>{name}</div>
        <div><h3>Height:</h3>{data.height}</div>
        <div><h3>Weight:</h3>{data.weight}</div>
        <div><h3>List of abilities:</h3></div>
        </div>
        <div className="ability">
        {
          data?.abilities?.map((abiliti)=>{
            return (
              <p> {abiliti.ability.name} </p>
            )
          })
        }
        </div>
      </CardContent>
    </StyledCard>
  );
};

CardComponent.defaultProps = {
  id: null,
  name: "",
  imageURL: "",
  listOfAbility:[],
  height:0,
  weight:0,
  onClick: { onclick }
};

export default CardComponent;
