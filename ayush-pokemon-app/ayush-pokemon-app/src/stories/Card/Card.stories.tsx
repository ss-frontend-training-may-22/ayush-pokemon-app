import { ComponentStory, ComponentMeta } from "@storybook/react";
import CardComponent from "./Card.component";

export default {
  title: "Form/Card",
  component: CardComponent,
  argTypes: {
    onClick: { action: "Clicked" },
  },
} as ComponentMeta<typeof CardComponent>;

const Template: ComponentStory<typeof CardComponent> = (args) => (
  <CardComponent {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  name: "ivysaur",
  url:""
};
