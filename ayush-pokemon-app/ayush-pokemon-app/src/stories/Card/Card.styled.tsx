import { Card } from "@mui/material";
import styled from "styled-components";

const StyledCard = styled(Card)`
    overflow: hidden;
    height: auto;
    font-family: Roboto, sans-serif;
    border-radius: 5px;
    box-shadow: rgb(0 0 0 / 5%) 0px 0px 20px, rgb(0 0 0 / 8%) 0px 0px 40px;
    width: 30%;
    padding: 20px;
    margin-right:5px;
    margin-top:5px;

.MuiCardContent-root{
  .ability{
    display: flex;
    justify-content: center;
    padding: 10px;
  p{
    padding: 10px;
    border: 1px solid;
    background-color: rgb(58, 176, 158);
    color: white;
  }
}
   .detail{
     div{
     display:flex; 
     justify-content: end;
     } 
   }
}
img{
  height:120px;
  width:120px;
}
`;

export default StyledCard;
