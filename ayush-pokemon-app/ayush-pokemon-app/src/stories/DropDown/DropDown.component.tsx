import * as React from 'react';
import Button from '@mui/material/Button';
import ChatIcon from '@mui/icons-material/Chat';
import StyledSelect from "./DropDown.styled";
import SearchIcon from '@mui/icons-material/Search';
import { FormControl, InputLabel, MenuItem, NativeSelect, Select } from '@mui/material';


type DropDownComponentProps = {
  variant: "text" | "outlined" | "contained" | undefined;
  size: "small" | "medium" | undefined;
  label:string;
  onClick?: React.MouseEventHandler;
};

const DropDownComponent = ({
  variant,
  size,
  label,
  onClick,
}: DropDownComponentProps) => {
  return (
    <>
      <StyledSelect >
      <FormControl sx={{ m: 1, minWidth: 60 }} >
      <Select
          defaultValue={10}
          inputProps={{ 'aria-label': 'Without label' }}
          size={size}
        >
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={20}>20</MenuItem>
          <MenuItem value={30}>30</MenuItem>
        </Select>
</FormControl>
      </StyledSelect>
    </>
  );
};

DropDownComponent.defaultProps = {
  variant: "primary",
  size: "medium",
  label:"",
  onClick: { onclick },
};

export default DropDownComponent;
