import { ComponentStory, ComponentMeta } from "@storybook/react";
import DropDownComponent from "./DropDown.component";
import SearchIcon from '@mui/icons-material/Search';

export default {
  title: "DropDownComponent",
  component: DropDownComponent,
  argTypes: {
    onClick: { action: "Clicked" },
  },
} as ComponentMeta<typeof DropDownComponent>;

const Template: ComponentStory<typeof DropDownComponent> = (args) => (
  <DropDownComponent {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  variant: "contained",
  size: "small",
  label: ""
};