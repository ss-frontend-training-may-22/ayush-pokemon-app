import { ComponentMeta, ComponentStory } from "@storybook/react";
import InputComponent from "./Input.component";

export default {
  title: "Input",
  component: InputComponent,
} as ComponentMeta<typeof InputComponent>;

const Template: ComponentStory<typeof InputComponent> = (args) => {
  return <InputComponent {...args} />;
};

export const PrimaryInput = Template.bind({});
PrimaryInput.args = {
  inputColor: "primary",
  inputLabel: "Name",
  hasFullWidth: false,
  inputType: "text",
};