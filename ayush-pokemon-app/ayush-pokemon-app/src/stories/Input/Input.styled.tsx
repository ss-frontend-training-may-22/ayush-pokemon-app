import { FormControl } from "@mui/material";
import styled from "styled-components";

type StyledFormControlProps = {
  width: string;
};

export const StyledFormControl = styled(FormControl)<StyledFormControlProps>`
  width: ${({ width }) => width};

  .MuiFormLabel-root {
    color: black;
    font-size: 18px;
    font-weight: 600;
  }

  .MuiInputBase-root {
    background: white;
    border: 1px solid black;
    font-size: 14px;
    margin-top: 22px;
    border-radius: 5px;

    .MuiInputBase-input {
      padding: 10px 12px;
    }

    &:hover {
      &:not(.Mui-disabled):before {
        border-bottom: none;
      }
    }

    &:after {
      border-bottom: none;
    }

    &:before {
      border-bottom: none;
    }
  }
`;